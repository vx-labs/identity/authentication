package services

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"errors"
	authorization "github.com/vx-labs/identity-api/authorization"
	types "github.com/vx-labs/identity-types/authorization"
	identity "github.com/vx-labs/identity/api"
	"os"
	"sync"
	"time"
)

const SESSIONS_CA = "sessions"

type SessionService interface {
	WithPoliciesID(ctx context.Context, uid string, kind string, policies []string, scope []string) (string, error)
	WithPoliciesRules(ctx context.Context, uid string, kind string, policies []string, scope []string) (string, error)
	Authority(ctx context.Context) *x509.Certificate
	Jwt(ctx context.Context) (string, error)
}

type sessionService struct {
	id      identity.Client
	authz   authorization.Client
	pkiName string
	ca      *x509.Certificate
	cert    *x509.Certificate
	key     *rsa.PrivateKey
	sync.Mutex
}

func Sessions(ctx context.Context, id identity.Client, authz authorization.Client) (SessionService, error) {
	s := &sessionService{id: id, authz: authz, pkiName: SESSIONS_CA}
	s.authz.WithJWTProvider(s)
	err := s.initialize(ctx)
	if err != nil {
		return nil, err
	}
	return s, nil
}

func (s *sessionService) Jwt(ctx context.Context) (string, error) {
	return s.id.CustomJwt("0", "service", s.cert, s.key, 10, []string{
		"users:*:*:1",
		"services:*:*:1",
		"authorization:*:*:1",
		"identity:*:*:1",
	}, []string{})
}
func (s *sessionService) renewCert() {
	ctx, c := context.WithCancel(context.Background())
	hostname, err := os.Hostname()
	if err != nil {
		return
	}
	defer c()
	pkiService := s.id.Certs(s.pkiName)
	reader := rand.Reader
	csr, err := pkiService.CreateCsr(s.key, reader, hostname, "VX", "identity")
	if err != nil {
		return
	}
	cert, err := pkiService.Create(ctx, csr)
	if err != nil {
		return
	}
	s.cert = cert
}

func (s *sessionService) initialize(ctx context.Context) error {
	ca, err := s.id.Cas().ById(ctx, s.pkiName)
	if err != nil {
		return errors.New("could not fetch CA")
	}
	hostname, err := os.Hostname()
	if err != nil {
		return errors.New("could not get hostname")
	}
	key, cert, err := s.id.Certs(s.pkiName).CreatePair(ctx, 2048, hostname, "VX", "identity")
	if err != nil {
		return errors.New("could not generate certificate: " + err.Error())
	}
	s.ca = ca
	s.key = key
	s.cert = cert
	go func() {
		t := time.Tick(24 * time.Hour)
		for {
			select {
			case <-t:
				now := time.Now()
				renewPeriod := 7 * 24 * time.Hour
				if s.cert.NotAfter.Sub(now) < renewPeriod {
					s.renewCert()
				}
			}
		}
	}()
	return nil
}
func (s *sessionService) Authority(ctx context.Context) *x509.Certificate {
	return s.ca
}
func (s *sessionService) WithScopes(ctx context.Context, uid string, kind string, scopes []string) (string, error) {
	return s.id.CustomJwt(uid, kind, s.cert, s.key, 300, []string{}, scopes)
}
func (s *sessionService) WithPoliciesID(ctx context.Context, uid string, kind string, policies []string, scope []string) (string, error) {
	rulesBag := []string{}
	for _, polId := range policies {
		pol, err := s.authz.Policies().ById(ctx, polId)
		if err != nil {
			break
		}
		for _, rule := range pol.Rules {
			for _, scopeName := range scope {
				if types.StringsMatch(scopeName, rule.Service) {
					rulesBag = append(rulesBag, rule.ToString())
				}
			}
		}
	}
	return s.WithPoliciesRules(ctx, uid, kind, rulesBag, scope)
}
func (s *sessionService) WithPoliciesRules(ctx context.Context, uid string, kind string, policies []string, scope []string) (string, error) {
	return s.id.CustomJwt(uid, kind, s.cert, s.key, 300, policies, scope)
}
