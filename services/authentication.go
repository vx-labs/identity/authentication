package services

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	types "github.com/vx-labs/identity-types/authentication"
	"golang.org/x/net/context/ctxhttp"
	"io"
	"net/http"
	"time"
)

type AuthenticationService interface {
	ByGoogle(ctx context.Context, token string) (types.GoogleClaim, error)
}

type authenticationService struct {
	logger     *logrus.Entry
	httpClient *http.Client
}

func Authentication() AuthenticationService {
	return &authenticationService{
		httpClient: &http.Client{Timeout: 1 * time.Second},
	}
}

func (service *authenticationService) ByGoogle(ctx context.Context, token string) (types.GoogleClaim, error) {
	claim := types.GoogleClaim{}
	if len(token) == 0 {
		return claim, errors.New("token must not be null")
	}
	validationResponse, err := service.validate(ctx, token)
	if err != nil {
		return claim, err
	}
	err = json.NewDecoder(validationResponse).Decode(&claim)
	defer validationResponse.Close()
	return claim, err
}

func (service *authenticationService) validate(ctx context.Context, token string) (io.ReadCloser, error) {
	url := fmt.Sprintf("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=%s", token)
	req, err := http.NewRequest("get", url, nil)
	if err != nil {
		return nil, err
	}
	response, err := ctxhttp.Do(ctx, service.httpClient, req)
	if err != nil {
		return nil, err
	} else if response.StatusCode != 200 {
		return nil, errors.New("token validation failed")
	} else {
		return response.Body, nil
	}
}
