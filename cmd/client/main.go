package main

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/vx-labs/authentication/services"
	clientAPI "github.com/vx-labs/go-rest-api/client"
	api "github.com/vx-labs/identity-api/authentication"
	"github.com/vx-labs/identity-api/authorization"
	identity "github.com/vx-labs/identity/api"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
)

func main() {
	ctx := context.Background()
	logger := clientAPI.NewLogger()
	logger.SetLevel(logrus.ErrorLevel)

	app := kingpin.New("auth", "vx authentication api client")
	role := app.Flag("role", "identity API role").String()
	secret := app.Flag("secret", "identity API secret").String()
	token := app.Flag("token", "identity API token - replace role and secret arguments.").String()

	appJWT := app.Command("jwt", "fetch JWT from API")
	appJWTCreate := appJWT.Command("create", "create JWT").Alias("ls")
	appJWTCreateRoot := appJWTCreate.Flag("root", "creates wildcard token").Default("false").Short('r').Bool()
	appJWTCreateScope := appJWTCreate.Flag("scope", "authorization scopes").Short('p').Strings()
	appJWTCreateShellSyntax := appJWTCreate.Flag("shell", "output token using shell's \"eval\" friendly").Default("false").Short('s').Bool()

	cmd := kingpin.MustParse(app.Parse(os.Args[1:]))

	switch cmd {
	case appJWTCreate.FullCommand():
		os.Setenv("APPROLE_ID", *role)
		if *appJWTCreateRoot {
			os.Setenv("VX_TOKEN", *token)
			id, err := identity.NewCustomClient(ctx, logger.WithField("source", "identity_client"), services.SESSIONS_CA)
			if err != nil {
				logger.Fatalf("could not create service api client: %s", err.Error())
			}
			client, err := api.NewCustomClient(ctx, logger.WithField("source", "auth_client"), id)
			if err != nil {
				logger.Fatalf("could not create service api client: %s", err.Error())
			}
			authz, err := authorization.NewCustomClient(ctx, logger.WithField("source", "authorization_client"))
			if err != nil {
				logger.Fatalf("could not create authorization api client: %s", err.Error())
			}
			authz.WithJWTProvider(client)
			s, err := services.Sessions(ctx, client.Identity(), authz)
			token, err := s.WithPoliciesRules(ctx, "_root", "user", []string{"*:*:*:31"}, *appJWTCreateScope)
			if err != nil {
				logger.Fatal(err.Error())
			}
			if *appJWTCreateShellSyntax {
				fmt.Printf("export VX_JWT=%s\n", token)
			} else {
				fmt.Println(token)
			}

		} else {
			os.Setenv("APPROLE_SECRET", *secret)
			client, err := api.NewClient(ctx, logger.WithField("source", "auth_client"))
			if err != nil {
				logger.Fatalf("could not create identity api client: %s", err.Error())
			}
			token, err := client.Jwt(ctx)
			if err != nil {
				logger.Fatal(err.Error())
			}
			if *appJWTCreateShellSyntax {
				fmt.Printf("export VX_JWT=%s\n", token)
			} else {
				fmt.Println(token)
			}
		}
	}
}
