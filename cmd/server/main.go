package main

import (
	"context"
	"fmt"
	"github.com/vx-labs/authentication/handlers"
	"github.com/vx-labs/authentication/services"
	api "github.com/vx-labs/go-rest-api/client"
	authAPI "github.com/vx-labs/identity-api/authentication"
	authzAPI "github.com/vx-labs/identity-api/authorization"
	servicesApi "github.com/vx-labs/services/api"
	usersApi "github.com/vx-labs/users/api"

	"os"
	"sync"
)

func main() {
	fmt.Println("starting authentication service")
	wg := sync.WaitGroup{}
	logger := api.NewLogger()
	ctx := context.Background()
	authn, err := authAPI.NewClient(ctx, logger.WithField("source", "identityClient"))
	if err != nil {
		logger.Errorf("could not connect to authentication service: %s", err.Error())
		os.Exit(1)
	}
	server, err := authn.NewServer(ctx, logger.WithField("source", "server"), "identity", "v1")
	if err != nil {
		logger.Errorf("could not spawn server: %s", err.Error())
		os.Exit(1)
	}
	authz, err := authzAPI.NewCustomClient(ctx, logger.WithField("source", "authzClient"))
	if err != nil {
		logger.Errorf("could not connect to authorization service: %s", err.Error())
		os.Exit(1)
	}
	authz.WithJWTProvider(authn)
	sessions, err := services.Sessions(ctx, authn.Identity(), authz)
	if err != nil {
		logger.Errorf(err.Error())
		os.Exit(1)
	}

	authentication := services.Authentication()
	wg.Add(1)
	var usersClient usersApi.Client
	var servicesClient servicesApi.Client
	go func(ctx context.Context) {
		defer wg.Done()
		var err error
		usersClient, err = usersApi.NewCustomClient(ctx, logger.WithField("source", "usersClient"))
		if err != nil {
			panic(err)
		}
		usersClient.WithJwtProvider(sessions)
	}(ctx)
	wg.Add(1)
	go func(ctx context.Context) {
		defer wg.Done()
		var err error
		servicesClient, err = servicesApi.NewCustomClient(ctx, logger.WithField("source", "servicesClient"))
		if err != nil {
			panic(err)
		}
		servicesClient.WithJwtProvider(sessions)
	}(ctx)
	wg.Wait()

	server.AddResource("/", handlers.NewTokensHandler(authn.Identity(), sessions, servicesClient.Services(), usersClient.Users(), authentication))
	server.AddResource("/", handlers.NewAuthorityHandler(sessions))
	server.AddResource("/", handlers.NewSessionHandler())
	server.ListenAndServe(ctx, 8006)
}
