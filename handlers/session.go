package handlers

import (
	"context"
	"github.com/vx-labs/go-rest-api"
	"io"
)

type sessionHandler struct {
}

func NewSessionHandler() api.Resource {
	return &sessionHandler{}
}

func (nh *sessionHandler) Grammar() api.Grammar {
	return api.Grammar{
		Plural:   "sessions",
		Singular: "session",
	}
}

func (nh *sessionHandler) List(ctx context.Context, payload io.ReadCloser) *api.Status {
	return api.Ok(api.Kv{"sessions": []string{"_self"}})
}

func (nh *sessionHandler) Read(ctx context.Context, payload io.ReadCloser) *api.Status {
	id, ok := api.Hierarchy(ctx)
	if !ok {
		return api.BadRequest(api.Kv{"error": "invalid instance"})
	}
	switch id.Id {
	case "_self":
		infos, ok := api.Identity(ctx)
		if ok {
			return api.Ok(api.Kv{"session": infos})
		} else {
			return api.InternalServerError(api.Kv{"error": "could not read session"})
		}
	}
	return api.Unsupported(api.Kv{"msg": "Unsupported operation"})
}

func (nh *sessionHandler) Create(ctx context.Context, payload io.ReadCloser) *api.Status {
	return api.Unsupported(api.Kv{"msg": "Unsupported operation"})
}

func (nh *sessionHandler) Update(ctx context.Context, payload io.ReadCloser) *api.Status {
	return api.Unsupported(api.Kv{"msg": "Unsupported operation"})
}

func (nh *sessionHandler) Delete(ctx context.Context, payload io.ReadCloser) *api.Status {
	return api.Unsupported(api.Kv{"msg": "Unsupported operation"})
}

func (nh *sessionHandler) Constraints() map[api.Verb]*api.Constraints {
	return map[api.Verb]*api.Constraints{
		api.LIST:   {Authentication: false},
		api.READ:   {Authentication: true},
		api.UPDATE: {Authentication: true},
		api.DELETE: {Authentication: true},
		api.CREATE: {Authentication: false},
	}
}

func (nh *sessionHandler) Children() []api.Resource {
	return nil
}
