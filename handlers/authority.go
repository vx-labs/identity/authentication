package handlers

import (
	"bytes"
	"context"
	"encoding/pem"
	"github.com/vx-labs/authentication/services"
	"github.com/vx-labs/go-rest-api"
	"io"
)

type authorityHandler struct {
	session services.SessionService
}

func NewAuthorityHandler(session services.SessionService) api.Resource {
	h := &authorityHandler{session: session}
	return h
}

func (nh *authorityHandler) Grammar() api.Grammar {
	return api.Grammar{
		Plural:   "authority",
		Singular: "authority",
	}
}

func (nh *authorityHandler) List(ctx context.Context, payload io.ReadCloser) *api.Status {
	return api.Ok(api.Kv{"authority": []string{"parsed", "pem"}})
}

func (nh *authorityHandler) Read(ctx context.Context, payload io.ReadCloser) *api.Status {
	id, ok := api.Hierarchy(ctx)
	if !ok {
		return api.BadRequest(api.Kv{"error": "invalid instance"})
	}
	switch id.Id {
	case "parsed":
		return api.Ok(api.Kv{"ca": nh.session.Authority(ctx)})
	case "pem":
		b := &pem.Block{Type: "CERTIFICATE", Bytes: nh.session.Authority(ctx).Raw}
		status := &api.Status{
			Code: 200,
			Raw:  bytes.NewReader(pem.EncodeToMemory(b)),
		}
		return status
	}
	return api.Unsupported(api.Kv{"msg": "Unsupported operation"})
}

func (nh *authorityHandler) Create(ctx context.Context, payload io.ReadCloser) *api.Status {
	return api.Unsupported(api.Kv{"msg": "Unsupported operation"})
}

func (nh *authorityHandler) Update(ctx context.Context, payload io.ReadCloser) *api.Status {
	return api.Unsupported(api.Kv{"msg": "Unsupported operation"})
}

func (nh *authorityHandler) Delete(ctx context.Context, payload io.ReadCloser) *api.Status {
	return api.Unsupported(api.Kv{"msg": "Unsupported operation"})
}

func (nh *authorityHandler) Constraints() map[api.Verb]*api.Constraints {
	return map[api.Verb]*api.Constraints{
		api.LIST:   {Authentication: false},
		api.READ:   {Authentication: false},
		api.UPDATE: {Authentication: true},
		api.DELETE: {Authentication: true},
		api.CREATE: {Authentication: true},
	}
}

func (nh *authorityHandler) Children() []api.Resource {
	return nil
}
