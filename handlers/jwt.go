package handlers

import (
	"context"
	"encoding/json"
	"github.com/vx-labs/authentication/services"
	"github.com/vx-labs/go-rest-api"
	types "github.com/vx-labs/identity-types/authentication"
	identity "github.com/vx-labs/identity/api"
	servicesApi "github.com/vx-labs/services/api"
	users "github.com/vx-labs/users/api"
	"io"
)

type tokensHandler struct {
	authentication services.AuthenticationService
	users          users.UsersClient
	services       servicesApi.ServicesClient
	session        services.SessionService
	identity       identity.Client
}

func NewTokensHandler(identity identity.Client,
	session services.SessionService,
	servicesClient servicesApi.ServicesClient,
	users users.UsersClient,
	authentication services.AuthenticationService,
) api.Resource {
	return &tokensHandler{
		services:       servicesClient,
		identity:       identity,
		authentication: authentication,
		session:        session,
		users:          users,
	}
}

func (nh *tokensHandler) Grammar() api.Grammar {
	return api.Grammar{
		Plural:   "tokens",
		Singular: "token",
	}
}

func (nh *tokensHandler) List(ctx context.Context, payload io.ReadCloser) *api.Status {
	return api.Ok(api.Kv{"login": []string{"google", "vx", "anonymous"}})
}

func (nh *tokensHandler) Read(ctx context.Context, payload io.ReadCloser) *api.Status {
	id, ok := api.Hierarchy(ctx)
	if !ok {
		return api.BadRequest(api.Kv{"error": "invalid instance"})
	}
	switch id.Id {
	case "self":
		infos, ok := api.Identity(ctx)
		if ok {
			return api.Ok(api.Kv{"self": infos})
		} else {
			return api.InternalServerError(api.Kv{"error": "could not read session"})
		}
	}
	return api.Unsupported(api.Kv{"msg": "Unsupported operation"})
}

func (nh *tokensHandler) Create(ctx context.Context, payload io.ReadCloser) *api.Status {
	logger := api.Logger(ctx)
	req := &types.Request{
		Provider: "anonymous",
	}
	if err := json.NewDecoder(payload).Decode(req); err != nil {
		logger.Errorf("failed to decode JSON: %s", err.Error())
		return api.Forbidden(api.Kv{"error": "access denied"})
	}
	var u types.Entity
	var kind string
	switch req.Provider {
	case "vx":
		kind = "service"
		claim, err := nh.identity.ValidateJwt(req.LoginToken)
		if err != nil {
			logger.Infof("refusing access to service: %s", err.Error())
			return api.Forbidden(api.Kv{"error": "access denied"})
		}
		service, err := nh.services.ByRoleId(ctx, claim.Id)
		if err != nil {
			logger.Infof("refusing access to service: %s", err.Error())
			return api.Forbidden(api.Kv{"error": "access denied"})
		}
		u = &service
	case "google":
		kind = "user"
		claim, err := nh.authentication.ByGoogle(ctx, req.LoginToken)
		if err != nil {
			logger.Infof("refusing access to user: %s", err.Error())
			return api.Forbidden(api.Kv{"error": "access denied"})
		}
		user, err := nh.users.ByGoogleId(ctx, claim.ClientId)
		if err != nil {
			logger.Infof("refusing access to user: %s", err.Error())
			return api.Forbidden(api.Kv{"error": "access denied"})
		}
		u = &user
	case "anonymous":
		token, err := nh.session.WithPoliciesRules(ctx, "_anonymous", "user", []string{
			"identity:tokens:_anonymous:1",
		}, []string{"identity"})
		if err != nil {
			logger.Infof("refusing access to user: %s", err.Error())
			return api.Forbidden(api.Kv{"error": "access denied"})
		}
		return api.Ok(api.Kv{"token": token})
	default:
		return api.Unsupported(api.Kv{"error": "invalid auth provider: '" + req.Provider + "'"})
	}

	token, err := nh.session.WithPoliciesID(ctx, u.UniqueId(), kind, u.AuthorizationPolicies(), req.Scope)
	if err != nil {
		logger.Infof("refusing access to user: %s", err.Error())
		return api.Forbidden(api.Kv{"error": "access denied"})
	}
	return api.Ok(api.Kv{"token": token})
}

func (nh *tokensHandler) Update(ctx context.Context, payload io.ReadCloser) *api.Status {
	return api.Unsupported(api.Kv{"msg": "Unsupported operation"})
}

func (nh *tokensHandler) Delete(ctx context.Context, payload io.ReadCloser) *api.Status {
	return api.Unsupported(api.Kv{"msg": "Unsupported operation"})
}

func (nh *tokensHandler) Constraints() map[api.Verb]*api.Constraints {
	return map[api.Verb]*api.Constraints{
		api.LIST:   {Authentication: false},
		api.READ:   {Authentication: true},
		api.UPDATE: {Authentication: true},
		api.DELETE: {Authentication: true},
		api.CREATE: {Authentication: false},
	}
}

func (nh *tokensHandler) Children() []api.Resource {
	return nil
}
